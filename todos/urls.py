from django.urls import path
from todos.views import TodoList


urlpatterns = [
    path("todos/", TodoList, name="todolist"),
]
